# after install Linux-Ubuntu

Los siguientes pasos son configuraciones personales sugeridas que se realizan sobre Ubuntu para personalizar el sistema operativo. 

## 1. Permitir descargas y actualizaciones del Universo, Multiverso y aquellas Restringidas 

Para garantizar que podamos descargar paquetes y programas oficiales de canonical (Main), aquellos creados por la comunidad bajo el concepto OpenSource (Universe), elementos restringidos o privados de terceros (Restricted) y aquellos que cuentan con derechos de autor (Multiverse), debemos seguir los siguientes pasos:

1. Abrir el dash board

2. Buscar el programa "Software and Updates"

3. Seleccionar la pestaña "Ubuntu Software"

4. Seleccionar las opciones anteriormente descritas

## 2. Actualizar nuestros paquetes de Ubuntu 

```
$ sudo apt-get update
```

En algunos casos también deberemos ejecutar las instrucciones: 

```
$ sudo apt-get dist-upgrade 
$ sudo apt-get upgrade 
```

## 3. Instalar los paquetes build-essential y el compilador CMake

Los dos (2) son paquetes necesarios para poder compilar códigos programados en lenguajes C y C++

```
$ sudo apt-get install build-essential
$ sudo apt-get install cmake
```

## 4. Instalar programas extras

Podemos instalar programas extras como drives para nuestro computador, editores de video y compresores .Zip y .rar


```
$ sudo apt-get install ubuntu-restricted-extras % mp3, audio, fuentes y otros extras
$ sudo apt-get install libavcodec-extra % para uso de editores de video
$ sudo apt-get install p7zip-full p7zip-rar rar unrar % se instala .zip y .rar para descomprimir archivos
```

## 5. Instalar chrome en Ubuntu

La versión OpenSource de Chrome para ubuntu se conoce como Chromium

```
$ sudo apt-get install chromium-browser
$ sudo apt-get install chromium-browser-l10n % paquete de idiomas (para tenerlo en español)
$ sudo apt-get update
$ sudo apt-get upgrade
```

## 6. Instalar el terminal Yakuake

Personalmente el mejor teminal de Ubuntu, adecuado para trabajar con ROS cuando requerimos abrir una gran cantidad de Shells al tiempo y organizarlos por grupos

```
$ sudo apt-get install yakuake
```

## 7. Instalar programas curiosos para el terminal 

oneko, un gato que persigue nuestro mouse

```
$ sudo apt-get install oneko
$ oneko % Ejecutar la aplicación
% <Ctrl> + 'C' para cerrar la aplicación o programa que se ejecuta de forma forzada 
```

sl, un tren que aparece en el terminal cuando nos equivocamos con la instrucción $ ls (listar un directorio)

```
$ sudo apt-get install sl
$ sl % Ejecutar la aplicación
$ LS % Otra forma de ejecutar la aplicación
```

cmatrix, un terminal con los simbolos de la película matrix

```
sudo apt-get install cmatrix
cmatrix % Ejecutar la aplicación
```

## 8. Instalar ROS (Robot Operating System)

Los pasos de instalación se pueden apreciar en la página oficial de ROS [ROS Instalation](http://wiki.ros.org/kinetic/Installation/Ubuntu) o en el siguiente video tutorial
[Video - ¿Cómo instalar ROS Kinetic Kame - 2016??](https://youtu.be/_vi190prtJw)

*** 

Realizado por   
Hernán Josué Hernández Lamprea  
hernan.hernandez01@ustabuca.edu.co  
Ing. Mecatrónico, Esp. Gerencia de proyectos  
Universidad Santo Tomás seccional Bucaramanga, Colombia